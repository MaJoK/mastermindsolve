pub fn find_solutions(guesses: &[Guess], colour_amount: u8) -> impl Iterator<Item = GuessRow> + '_ {
    codegen::CombinationGenerator::new(colour_amount).filter(move |row| {
        for guess in guesses {
            if !guess.possible_solution(row) {
                return false;
            }
        }
        true
    })
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub struct Colour {
    number: u8,
}

impl Colour {
    pub fn new(number: u8) -> Self {
        assert!(number < 32, "Number can not be larger than 31");
        Self { number }
    }

    pub fn as_bitset(&self) -> i32 {
        1 << self.number
    }

    pub fn number(&self) -> u8 {
        self.number
    }
}

pub struct RowComparision {
    same_colors: u8,
    same_positions: u8
}

impl RowComparision {
    pub fn colors_in_wrong_position(&self) -> u8 {
        self.same_colors - self.same_positions
    }
    pub fn same_colors(&self) -> u8 {
        self.same_colors
    }
    pub fn same_positions(&self) -> u8 {
        self.same_positions
    }
}

#[derive(Debug)]
pub struct GuessRow {
    colours: [Colour; 4],
    joined_colours: i32,
}

impl GuessRow {
    pub fn new(colours: [Colour; 4]) -> Self {
        Self {
            joined_colours: join_colours(&colours),
            colours,
        }
    }

    pub fn compare(&self, other: &Self) -> RowComparision {
        RowComparision {
            same_colors: self.count_same_colours(other),
            same_positions: self.count_same_positions(other),
        }
    }

    fn count_same_colours(&self, other: &Self) -> u8 {
        (self.joined_colours & other.joined_colours).count_ones() as u8
    }

    fn count_same_positions(&self, other: &Self) -> u8 {
        self.colours
            .iter()
            .zip(other.colours.iter())
            .filter(|(left, right)| left == right)
            .count() as u8
    }

    pub fn colours(&self) -> &[Colour; 4] {
        &self.colours
    }
}

impl PartialEq for GuessRow {
    fn eq(&self, other: &Self) -> bool {
        self.colours.eq(&other.colours)
    }
}

pub struct Guess {
    correct_colours: u8,
    correct_colours_and_positions: u8,
    guessed_colours: GuessRow,
}

impl Guess {
    pub fn new(
        correct_colours: u8,
        correct_colours_and_positions: u8,
        guessed_colours: [Colour; 4],
    ) -> Self {
        assert!(
            correct_colours + correct_colours_and_positions <= 4,
            "Can't have more than 4 colours and positions right if you can only guess 4"
        );
        Self {
            correct_colours,
            correct_colours_and_positions,
            guessed_colours: GuessRow::new(guessed_colours),
        }
    }

    fn possible_solution(&self, row: &GuessRow) -> bool {
        let comparision = self.guessed_colours.compare(row);
        comparision.same_colors() == self.total_colours_correct() && comparision.same_positions() == self.correct_colours_and_positions
    }

    fn total_colours_correct(&self) -> u8 {
        self.correct_colours + self.correct_colours_and_positions
    }
}

fn join_colours(colours: &[Colour; 4]) -> i32 {
    colours[0].as_bitset()
        | colours[1].as_bitset()
        | colours[2].as_bitset()
        | colours[3].as_bitset()
}

#[cfg(test)]
mod test {
    use super::{Colour, Guess, GuessRow};

    #[test]
    fn finds_correct_solution() {
        let colours = [
            Colour::new(0),
            Colour::new(1),
            Colour::new(2),
            Colour::new(3),
        ];

        let solution = vec![GuessRow::new(colours)];

        let guess = Guess::new(0, 4, colours);

        let found_solutions: Vec<GuessRow> = super::find_solutions(&[guess], 5).collect();

        assert_eq!(solution, found_solutions)
    }

    #[test]
    fn finds_correct_solution2() {
        let one = Colour::new(0);
        let two = Colour::new(1);
        let three = Colour::new(2);
        let four = Colour::new(3);
        let five = Colour::new(4);
        let six = Colour::new(5);

        let solutions = vec![
            GuessRow::new([one, two, six, three]),
            GuessRow::new([one, six, three, two]),
            GuessRow::new([six, two, three, one]),
        ];

        let guess1 = Guess::new(1, 2, [one, two, three, four]);
        let guess2 = Guess::new(1, 2, [one, two, three, five]);

        assert_eq!(
            solutions,
            super::find_solutions(&[guess1, guess2], 6).collect::<Vec<_>>()
        );
    }
}

mod codegen {
    use super::{Colour, GuessRow};

    pub struct CombinationGenerator {
        amount_of_colours: u8,
        next_combination: [u8; 4],
        combos_left: usize,
    }

    impl CombinationGenerator {
        pub fn new(amount_of_colours: u8) -> Self {
            assert!(amount_of_colours >= 4, "Because combinations with 4 different colours are generated at least 4 colours need to be present");
            Self {
                amount_of_colours,
                next_combination: [0, 1, 2, 3],
                combos_left: Self::combinations(amount_of_colours),
            }
        }

        fn combinations(amount_of_colours: u8) -> usize {
            let amount_of_colours = amount_of_colours as usize;
            let mut amount: usize = 1;
            for num in amount_of_colours - 3..=amount_of_colours {
                amount = amount.checked_mul(num).unwrap();
            }
            amount
        }

        fn gen_next(&mut self) -> [u8; 4] {
            let colour = self.next_combination.clone();
            self.increment_index(3);
            self.combos_left -= 1;
            colour
        }

        fn increment_index(&mut self, index: isize) {
            if index < 0 {
                return;
            }
            let index_pos = index as usize;
            if self.next_combination[index_pos] + 1 == self.amount_of_colours {
                self.increment_index(index - 1);
                self.next_combination[index_pos] = 0;
            } else {
                self.next_combination[index_pos] += 1;
            }

            // No duplicate numbers
            if self.next_combination[0..index_pos].contains(&self.next_combination[index_pos]) {
                self.increment_index(index);
            }
        }
    }

    impl Iterator for CombinationGenerator {
        type Item = GuessRow;

        fn next(&mut self) -> Option<Self::Item> {
            if self.combos_left > 0 {
                Some(into_guess_arr(&self.gen_next()))
            } else {
                None
            }
        }
    }

    fn into_guess_arr(colours: &[u8; 4]) -> GuessRow {
        let colours = [
            Colour::new(colours[0]),
            Colour::new(colours[1]),
            Colour::new(colours[2]),
            Colour::new(colours[3]),
        ];
        GuessRow::new(colours)
    }

    #[cfg(test)]
    mod test {
        use super::*;

        fn gen_guesses(colour_amount: u8) -> Vec<GuessRow> {
            let colour_amount = colour_amount as usize;
            let mut raw_guesses: Vec<[u8; 4]> = Vec::new();
            raw_guesses.resize_with(colour_amount.pow(4), Default::default);
            gen_guesses1(colour_amount, &mut raw_guesses[..]);
            raw_guesses
                .iter()
                .filter(|row| {
                    !row[0..1].contains(&row[1])
                        && !row[0..2].contains(&row[2])
                        && !row[0..3].contains(&row[3])
                })
                .map(|row| into_guess_arr(row))
                .collect()
        }

        fn gen_guesses1(colour_amount: usize, codes: &mut [[u8; 4]]) {
            let blocksize = colour_amount.pow(3);
            for color in 0..colour_amount {
                let block = &mut codes[color * blocksize..(color + 1) * blocksize];
                for combination in &mut *block {
                    combination[0] = color as u8;
                }
                gen_guesses2(colour_amount, &mut *block);
            }
        }

        fn gen_guesses2(colour_amount: usize, codes: &mut [[u8; 4]]) {
            let blocksize = colour_amount.pow(2);
            for color in 0..colour_amount {
                let block = &mut codes[color * blocksize..(color + 1) * blocksize];
                for combination in &mut *block {
                    combination[1] = color as u8;
                }
                gen_guesses3(colour_amount, &mut *block);
            }
        }

        fn gen_guesses3(colour_amount: usize, codes: &mut [[u8; 4]]) {
            let blocksize = colour_amount;
            for color in 0..colour_amount {
                let block = &mut codes[color * blocksize..(color + 1) * blocksize];
                for combination in &mut *block {
                    combination[2] = color as u8;
                }
                gen_guesses4(colour_amount, &mut *block);
            }
        }

        fn gen_guesses4(colour_amount: usize, codes: &mut [[u8; 4]]) {
            for color in 0..colour_amount {
                codes[color][3] = color as u8;
            }
        }

        #[test]
        fn test_generation() {
            let generated_colours: Vec<GuessRow> = CombinationGenerator::new(8).collect();
            let expected_combinations = gen_guesses(8);
            assert_eq!(expected_combinations, generated_colours);
        }

        #[test]
        fn test_combination_calc() {
            assert_eq!(10 * 9 * 8 * 7, CombinationGenerator::combinations(10));
            assert_eq!(8 * 7 * 6 * 5, CombinationGenerator::combinations(8));
            assert_eq!(4 * 3 * 2 * 1, CombinationGenerator::combinations(4));
            assert_eq!(15 * 14 * 13 * 12, CombinationGenerator::combinations(15));
        }
    }
}
